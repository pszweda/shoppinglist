<?php 

class Lists extends Eloquent {

	protected $table = 'lists';

	public function getItems()
    {
        return $this->hasMany('ListItem', 'list_id', 'link');
    }
}