<?php

class InstallController extends BaseController {

	public function install()
	{
		if(!Schema::hasTable('users'))
			Schema::create('users', function($table)
			{
			     $table->increments('id');
		         $table->string('username', 45);
		         $table->string('email', 80)->uniqid();
		         $table->string('facebook', 80)->uniqid()->nullable();
		         $table->string('twitter', 80)->uniqid()->nullable();
		         $table->string('google', 80)->uniqid()->nullable();
		         $table->string('password', 80);
                 $table->string('remember_token', 100)->nullable();
                 $table->timestamps();
                 $table->softDeletes();
			});

		if(!Schema::hasTable('lists'))
			Schema::create('lists', function($table)
			{
			     $table->increments('id');
			     $table->string('link', 20);
		         $table->string('name', 300);
		         $table->text('description');
                 $table->timestamps();
                 $table->softDeletes();
			});



		if(!Schema::hasTable('list_items'))
			Schema::create('list_items', function($table)
			{
			     $table->increments('id');
			     $table->integer('list_id')->unsigned();
		         $table->string('what', 350);
		         $table->string('from', 300);
		         $table->integer('quantity')->default(1);
                 $table->timestamps();
                 $table->softDeletes();
			});		

		if(!Schema::hasTable('list_items'))
			Schema::table('list_items', function($table) {
                 $table->foreign('list_id')->references('link')->on('lists');
             });
		
		return "Tabele w bazie zostały utworzone poprawnie";
	}

}
