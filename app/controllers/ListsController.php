<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ListsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function showLists()
    {
        $items = Lists::orderBy('id', 'desc')->get()->take(10);

        return View::make('addNewList')->with(array('items' => $items));
    }

    public function addNewList() 
    {

    	$title = Input::get('title');

    	if(strlen($title) > 3)
    	{
			$item = new Lists; 

			$item->name = $title;
			$item->link = time();
			$item->description = "";

			$item->save();

			$items = Lists::orderBy('id', 'desc')->get()->take(10);

			return View::make('addNewList')->with(array('items' => $items, 'message' => 'Lista została stworzona'));
    	}
    	else
    	{
    		$items = Lists::orderBy('id', 'desc')->get()->take(10);
    		return View::make('addNewList')->with(array('items' => $items, 'message' => 'Podana nazwa jest za krótka!'));
    	}
    	
    }

	public function showListItems($id, $show = 'none')
	{
		$list = Lists::where('link', '=', $id)->firstOrFail();
		$items = $list->getItems;
		$from = $_COOKIE["from"];

		return View::make('lista')->with(array('items' => $items, 'from' => $from, 'pageId' => $id, 'show' => $show));
	}

	public function addListItem($id) {
		$what = Input::get('what');
		$from = Input::get('from');
		$quantity = Input::get('quantity');

		if(strlen($what) > 2)
		{
			$item = new ListItem;
			$item->list_id = $id;
			$item->what = $what;
			$item->from = $from;
			$item->quantity = $quantity;

			$item->save();
			return Redirect::to('/' . $id)->with(array('message' => 'Dodano produkt do listy'));
		}
		else
		{
			return Redirect::to('/' . $id)->with(array('message' => 'Podana nazwa produktu jest za którka!'));
		}	
	}

}
