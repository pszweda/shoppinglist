<?php

class UserController extends BaseController {

    public function Register()
    {
        $email = Input::get('email');
        $password = Input::get('pass');

        if (Auth::attempt(array('email' => $email, 'password' => $password), true))
        {
            Cookie::queue('from', Auth::user()->username, time() + 3600);
            return Redirect::to('');
        }
        else
        {
            if(!is_null(Input::get('username')) && !is_null(Input::get('email')) && strlen(Input::get('pass')) > 5 )
            {
                $username = Input::get('username');
                $email = Input::get('email');
                $clearpass = Input::get('pass');
                $password = Hash::make(Input::get('pass'));

                $user = new User;

                $user->username = $username;
                $user->email = $email;
                $user->password = $password;

                $user->save();

                if (Auth::attempt(array('email' => $email, 'password' => $clearpass), true))
                {
                    Cookie::queue('from', Auth::user()->username, time() + 3600);
                    return Redirect::to('');
                }
            }
        }
    }

    public function Login()
    {
        $email = Input::get('email');
        $password = Input::get('pass');
        if (Auth::attempt(array('email' => $email, 'password' => $password), true))
        {
            Cookie::queue('from', Auth::user()->username, time() + 3600);
            return Redirect::to('');
        }
        else
        {
            return Redirect::to('')->with(array('message' => 'Błędne dane!'));
        }
    }

}
