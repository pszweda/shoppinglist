<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ListsController@showLists');
Route::post('/', 'ListsController@addNewList');
Route::get('/install', 'InstallController@install');
Route::get('/{id}', 'ListsController@showListItems');
Route::get('/{id}/{show}', 'ListsController@showListItems');

Route::post('/register', 'UserController@Register');
Route::post('/login', 'UserController@Login');
Route::post('/{id}', 'ListsController@addListItem');
