@extends('master')
@section('content')
 
      <div class="row">
        <div class="large-12 columns">
          <div class="row">
             <br>
            <form method="post">
              <div class="row collapse">
                <div class="small-3 large-2 columns">
                  <span class="prefix">Kto:</span>
                </div>
                <div class="small-9 large-10 columns">
                  <input name="from" type="text" placeholder="Podpisz się..." readonly="true" value="{{$from}}">
                </div>
              </div>
              <div class="row collapse">
                <div class="small-3 large-2 columns">
                  <span class="prefix">Co:</span>
                </div>
                <div class="small-9 large-10 columns">
                  <input name="what" type="text" placeholder="Wpisz co mam kupić...">
                </div>
              </div>
              <input name="quantity" type="hidden" value="1">
              <button type="submit" class="button small radius right">Dodaj</button>
            </form>
 
          </div>
        </div>
      </div>


 	<div class="row">
        <div class="large-12 columns">
        	<h2>
                @if($show !== 'all')
                    Rzeczy, które dodałeś do listy:
                @else
                    Lista:
                @endif
            </h2>
        	<table class="large-12">
        	  <thead>
        	    <tr>
        	      <th width="50">Lp.</th>
        	      <th>Co</th>
        	      <th width="150">Kto</th>
        	      <th width="50">Ile</th>
        	    </tr>
        	  </thead>
        	  <tbody>

        	  	<?php $i=1; ?>
		
				@foreach ($items as $item)
                    @if ($item->from == $from || $show == 'all')
                        <tr>
                          <td>{{ $i }}</td>
                          <td>{{ $item->what }}</td>
                          <td>{{ $item->from }}</td>
                          <td>{{ $item->quantity }}</td>
                        </tr>
                        <?php $i++; ?>
                    @endif
				@endforeach

        	    
        	  </tbody>
        	</table>
        </div>
    </div>

@stop
