<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Shopping List</title>
    <link rel="stylesheet" href="<?php echo asset('assets/css/foundation.css'); ?>" />
    <link rel="stylesheet" href="<?php echo asset('assets/css/main.css'); ?>" />
    <script src="<?php echo asset('assets/js/vendor/modernizr.js'); ?>"></script>

</head>
<body>

    @if (!Auth::check())

    <script>

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1422642621355286',
          xfbml      : true,
          version    : 'v2.0'
        });

        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            console.log('Logged in.');
            testAPI();
          }
          else {
            $('#myModal').foundation('reveal', 'open');
            $("form :input").prop("disabled", true);
            $("form :button").prop("disabled", true);
            $('#register :input, #register :button').prop('readon', false);
            $('#login :input, #login :button').prop('disabled', false);
            //FB.login();
          }
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/pl_PL/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));


      function testAPI() {
            console.log('Welcome!  Fetching your information.... ');
            if(getCookie('from').length > 0)
            {
                $('input[name="from"]').val(getCookie('from'));
            }
            else
            {
              FB.api('/me', function(response) {
                $('input[name="from"]').val(response.first_name + ' ' + response.last_name);
                $('input[name="from"]').prop('readonly', true);
                setCookie('from', response.first_name + ' ' + response.last_name, 30);
                location.reload();
              });
            }
            
          }
    </script>

    @endif

<nav class="top-bar" data-topbar>
    <ul class="title-area">

        <li class="name">
            <h1>
                <a href="/">
                    Shopping List

                    @if (Auth::check())
                        (Witaj {{ Auth::user()->username; }})
                    @elseif (isset($from))
                        (Witaj {{ $from }})
                    @endif
                </a>
            </h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>


</nav>

@if (Session::get('message'))
<div class="alert alert-error">{{Session::get('message')}}</div>
@endif

<?php $masterId = (isset($pageId))? $pageId : '' ;?>

<div id="myModal" class="reveal-modal small" data-reveal>
    <h3>Zaloguj się!</h3>
    <p>
      <a class="button small radius " href="https://www.facebook.com/dialog/oauth?client_id=1422642621355286&redirect_uri=http://shopping.szweda.com.pl/<?php echo $masterId; ?>">facebook</a>
      <a class="button small success radius  " href="#" onclick="alert('Jeszcze nie ma tej opcji.')">Twitter</a>
      <a class="button small alert radius " href="#" onclick="alert('Jeszcze nie ma tej opcji.')">Google</a>
      <a class="button small secondary radius " href="#email">Adres email</a>
    </p>
    <hr>
    <div class="register">
        <h3>lub zarejestruj się!</h3>
        <form id="register" action="register" method="post">
            <div class="row">
                <div class="large-12 columns">
                    <label>Nazwa użytkownika
                        <input name="username" type="text" placeholder="Wpisz nazwę użytkownika" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Email
                        <input name="email" type="text" placeholder="Wpisz adres email" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Hasło
                        <input name="pass" type="password"/>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <button class="button small radius" type="submit">Stwórz konto</button>
                </div>
            </div>
        </form>
    </div>
    <div class="login" style="display: none;">
        <form id="login" action="login" method="post">
            <div class="row">
                <div class="large-12 columns">
                    <label>Email
                        <input name="email" type="text" placeholder="Wpisz adres email" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <label>Hasło
                        <input name="pass" type="password"/>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <button class="button small radius" type="submit">Zaloguj się!</button>
                </div>
            </div>
        </form>
    </div>
  <a class="close-reveal-modal">&#215;</a>
</div>