@extends('master')
@section('content')
 
      <div class="row">
        <div class="large-12 columns">
          <div class="row">
             <br>
            <form method="post">
              <div class="row collapse">
                <div class="small-3 large-2 columns">
                  <span class="prefix">Nazwa listy:</span>
                </div>
                <div class="small-9 large-10 columns">
                  <input name="title" type="text" placeholder="Wpisz jakąś nazwę" value="">
                </div>
              </div>
              <button type="submit" class="button small radius right">Stwórz</button>
            </form>
 
          </div>
        </div>
      </div>


        <div class="row">
            <div class="large-12 columns">
              <h2>Lista:</h2>
              <table class="large-12">
                <thead>
                  <tr>
                    <th width="50">Lp.</th>
                    <th>Lista</th>
                    <th style="width: 200px">Data utworzenia</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $i=1; ?>
                
                    @foreach ($items as $item)
                        <tr>
                          <td>{{ $i }}</td>
                          <td><a href="{{$item->link}}">{{ $item->name }}</a></td>
                          <td>{{ $item->created_at }}</td>
                        </tr>

                         <?php $i++; ?>
                    @endforeach

                  
                </tbody>
              </table>
            </div>
        </div>

@stop
