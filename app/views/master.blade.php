@include('header')

<div class="row">
    <div class="large-12 columns">

    @yield('content')

    </div>
</div>

@include('footer')